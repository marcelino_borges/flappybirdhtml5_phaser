// create a new scene named "Game"
//let gameScene = new Phaser.Scene('Game');
let canvasWidth = 800, canvasHeight = 600;
// our game's configuration
let config = {
    type: Phaser.AUTO,  //Phaser will decide how to render our game (WebGL or Canvas)
    width: canvasWidth, // game width
    height: canvasHeight, // game height
    scene: {
        preload: preload,
        create: create,
        update: update
    } // our newly created scene
};

// create the game, and pass it the configuration
let game = new Phaser.Game(config);


function preload() {
    this.load.image("bg", "./assets/bg.png");
    this.load.image("pipeTop", "./assets/pipe_top.png");
    this.load.image("pipeBottom", "./assets/pipe_bottom.png");
    this.load.image("bird", "./assets/bird_spritesheet.png", { frameWidth: 84, frameHeight: 54 });

}

function create() {
    this.add.image(canvasWidth / 2, canvasHeight / 2, "bg");
    this.add.image(650, 0, "pipeTop");
    this.add.image(650, canvasHeight, "pipebottom");
    this.add.image(canvasWidth / 2, canvasHeight / 2, "bird");
}

function update() {

}